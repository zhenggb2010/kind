package com.kind.perm.web.account;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kind.common.client.response.JsonResponseResult;
import com.kind.common.contants.Constants;
import com.kind.common.dto.DataGridResult;
import com.kind.common.enums.ResponseState;
import com.kind.common.exception.ServiceException;
import com.kind.common.persistence.PageView;
import com.kind.common.uitls.DateUtils;
import com.kind.common.uitls.NumberUtils;
import com.kind.perm.core.account.domain.AccountDO;
import com.kind.perm.core.account.service.AccountService;
import com.kind.perm.core.system.domain.TableCustomTempletDO;
import com.kind.perm.core.system.service.TableCustomService;
import com.kind.perm.core.system.service.TableCustomTempletService;
import com.kind.perm.web.common.ExcelUtils;
import com.kind.perm.web.common.controller.BaseController;

/**
 *
 * 用户信息管理控制器. <br/>
 *
 * @date:2016年5月12日 上午11:18:52 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Controller
@RequestMapping("account/account")
public class AccountController extends BaseController {

	@Autowired
	private AccountService accountService;
	
	@Autowired
	private TableCustomService tableCustomService;
	
	@Autowired
	private TableCustomTempletService tableCustomTempletService;

	/**列表页面*/
	private final String LIST_PAGE = "account/account_list";
	/**表单页面*/
	private final String FORM_PAGE = "account/account_form";

	/**
	 * 进入到默认列表页面
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String toListPage() {
		return LIST_PAGE;
	}

	/**
	 * 全部公司数据.
	 *
	 * @return
	 */
	@RequestMapping(value = "getAccountTree", method = RequestMethod.GET)
	@ResponseBody
	public List<AccountDO> getAllMenus() {
		List<AccountDO> accountList = accountService.queryList(new AccountDO());
		return accountList;
	}

	/**
	 * 获取分页查询列表数据.
	 */
	// @RequiresPermissions("sys:user:view")
	@RequestMapping(value = "selectPageList", method = RequestMethod.GET)
	@ResponseBody
	public DataGridResult selectPageList(AccountDO query, HttpServletRequest request) {
		PageView<AccountDO> page = accountService.selectPageList(query);
		return super.buildDataGrid(page);
	}

	/**
	 * 加载添加页面.
	 *
	 * @param model
	 */
	@RequiresPermissions("account:account:save")
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String toAdd(Model model) {
		model.addAttribute("entity", new AccountDO());
		model.addAttribute(Constants.CONTROLLER_ACTION, Constants.CONTROLLER_ACTION_ADD);
		return FORM_PAGE;
	}

	/**
	 * 保存数据.
	 *
	 * @param entity
	 * @param model
	 */
	@RequestMapping(value = "add", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponseResult add(@Valid AccountDO entity, Model model) {
		try {
			entity.setRegisterDate(new Date());
			accountService.save(entity);
			return JsonResponseResult.success();

		}catch (ServiceException e){
			e.printStackTrace();
			logger.error(e.getMessage());
			return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
		}
	}

	/**
	 * 加载修改页面.
	 *
	 * @param id
	 * @param model
	 * @return
	 */
	@RequiresPermissions("account:account:change")
	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String update(@PathVariable("id") Long id, Model model) {
		updateView(id, model);
		return FORM_PAGE;
	}

	/**
	 * 加载查看页面.
	 *
	 * @param id
	 * @param model
	 * @return
	 */
	@RequiresPermissions("account:account:view")
	@RequestMapping(value = "view/{id}", method = RequestMethod.GET)
	public String view(@PathVariable("id") Long id, Model model) {
		updateView(id, model);
		model.addAttribute("view", "true");
		return FORM_PAGE;
	}
	


	/**
	 * 修改数据.
	 *
	 * @param entity
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponseResult update(@Valid @ModelAttribute @RequestBody AccountDO entity, Model model) {
		try {
			accountService.save(entity);
			return JsonResponseResult.success();

		}catch (ServiceException e){
			e.printStackTrace();
			logger.error(e.getMessage());
			return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
		}
	}

	/**
	 * 删除数据.
	 *
	 * @param id
	 * @return
	 */
	@RequiresPermissions("account:account:remove")
	@RequestMapping(value = "remove/{id}")
	@ResponseBody
	public JsonResponseResult remove(@PathVariable("id") Long id) {
		try {
			if (!NumberUtils.isEmptyLong(id)) {
				accountService.remove(id);
			}
			return JsonResponseResult.success();

		}catch (ServiceException e){
			e.printStackTrace();
			logger.error(e.getMessage());
			return JsonResponseResult.build(ResponseState.ERROR_CODE_SERVICE_ERROR.getCode(), e.getMessage());
		}
	}
	
	/**
	 * 导出信息
	 * @param ids
	 * @param request
	 * @param response
	 * @return
	 */
	@RequiresPermissions("account:account:export")
	@RequestMapping("export")
	@ResponseBody
	public String export(AccountDO query,HttpServletRequest request,HttpServletResponse response){
		TableCustomTempletDO entity = new TableCustomTempletDO();
		entity.setJavaBean("CommunityDO");
		List<TableCustomTempletDO> tableCustomTempletlist = tableCustomTempletService.queryList(entity);
		TableCustomTempletDO tableCustomTempletDO = new TableCustomTempletDO();
		if(tableCustomTempletlist.size()>0)
		{
			tableCustomTempletDO = tableCustomTempletlist.get(0);
		}
		else
		{
			return "未设置信息";
		}
		return ExcelUtils.export(accountService.queryList(query), tableCustomService.findTableCustomExport(tableCustomTempletDO.getType()), tableCustomTempletDO.getName()+DateUtils.getDateRandom()+".xls", request, response);
	
	}
	
	
	
	private void updateView(Long id, Model model) {
		model.addAttribute("entity", accountService.getById(id));
		model.addAttribute(Constants.CONTROLLER_ACTION, Constants.CONTROLLER_ACTION_UPDATE);
	}

}
