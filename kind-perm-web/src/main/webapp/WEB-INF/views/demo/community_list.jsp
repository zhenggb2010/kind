<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/easyui.jsp"%>
<script src="${ctx}/static/plugins/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
</head>

<body>
    <div id="tb" style="padding:5px;height:auto">
        <div>
            <form id="searchFrom" action="">
                <input type="text" name="cityName" class="easyui-validatebox" data-options="width:150,prompt: '市'"/>
                <input type="text" name="areaName" class="easyui-validatebox" data-options="width:150,prompt: '区'"/>
                <input type="text" name="community" class="easyui-validatebox" data-options="width:150,prompt: '小区名称'"/>
                <input type="text" name="beginDate" class="easyui-my97" datefmt="yyyy-MM-dd" data-options="width:150,prompt: '开始日期'"/>
                - <input type="text" name="endDate" class="easyui-my97" datefmt="yyyy-MM-dd" data-options="width:150,prompt: '结束日期'"/>
                <span class="toolbar-item dialog-tool-separator"></span>
                <input type="hidden" name="order"/>
                <input type="hidden" name="sort"/>
                <a href="javascript(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="searchFunc();">查询</a>
            </form>
            <shiro:hasPermission name="demo:community:save">
	            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addFunc();">添加</a>
	            <span class="toolbar-item dialog-tool-separator"></span>
            </shiro:hasPermission>
            <shiro:hasPermission name="demo:community:change">
	            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="updateFunc();">修改</a>
	            <span class="toolbar-item dialog-tool-separator"></span>
            </shiro:hasPermission>
            <shiro:hasPermission name="demo:community:view">
	            <a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-search" onclick="view()">查看</a>
			    <span class="toolbar-item dialog-tool-separator"></span>
		    </shiro:hasPermission>
		    <shiro:hasPermission name="demo:community:remove">
	            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeFunc();">删除</a>
	            <span class="toolbar-item dialog-tool-separator"></span>
            </shiro:hasPermission>
             <shiro:hasPermission name="demo:community:export">
	            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-hamburg-home" plain="true" onclick="exportFunc();">导出</a>
	            <span class="toolbar-item dialog-tool-separator"></span>
            </shiro:hasPermission>
        </div>
    </div>

    <!--列表表格-->
    <table id="dataTable"></table>
    <!--添加、修改窗体-->
    <div id="dlg"></div>

<script type="text/javascript">
    var dg;
    var d;

    $(function(){
        dg=$('#dataTable').datagrid({
            method: "get",
            url:'${ctx}/demo/community/selectPageList',
            fit : true,
            fitColumns : true,
            border : false,
            idField : 'id',
            striped:true,
            pagination:true,
            rownumbers:true,
            pageNumber:1,
            remoteSort: true,
            rownumbers: true,
            pageSize : 20,
            pageList : [20],
            singleSelect:true,
            columns:[[
                {field:'id',title:'id',hidden:true},
                {field:'community',title:'小区名称',width:20,sortable:true},
                {field:'cityName',title:'市',width:10},
                {field:'cityCode',title:'市编码',width:10},
                {field:'areaName',title:'区',width:10},
                {field:'areaCode',title:'区编码',width:10,sortable:true},
                {field:'createDate',title:'时间',width:10}

            ]],
            headerContextMenu: [
                {
                    text: "冻结该列", disabled: function (e, field) { return dg.datagrid("getColumnFields", true).contains(field); },
                    handler: function (e, field) { dg.datagrid("freezeColumn", field); }
                },
                {
                    text: "取消冻结该列", disabled: function (e, field) { return dg.datagrid("getColumnFields", false).contains(field); },
                    handler: function (e, field) { dg.datagrid("unfreezeColumn", field); }
                }
            ],
            enableHeaderClickMenu: true,
            enableHeaderContextMenu: true,
            enableRowContextMenu: false,
            toolbar:'#tb'
        });
    });

    //弹窗增加
    function addFunc() {
        d=$("#dlg").dialog({
            title: '添加小区信息',
            width: 700,
            height: 500,
            href:'${ctx}/demo/community/add',
            maximizable:true,
            modal:true,
            buttons:[{
                text:'确认',
                class: 'easyui-linkbutton c6',
                iconCls: 'icon-ok',
                handler:function(){
                    $("#mainform").submit();
                }
            },{
                text:'取消',
                iconCls: 'icon-cancel',
                handler:function(){
                    d.panel('close');
                }
            }]
        });
    }

    //弹窗修改
    function updateFunc(){
        var row = dg.datagrid('getSelected');
        if(rowIsNull(row)) return;
        d=$("#dlg").dialog({
            title: '修改小区信息',
            width: 700,
            height: 500,
            href:'${ctx}/demo/community/update/'+row.id,
            maximizable:true,
            modal:true,
            buttons:[{
                text:'修改',
                iconCls: 'icon-ok',
                handler:function(){
                    $('#mainform').submit();
                }
            },{
                text:'取消',
                iconCls: 'icon-cancel',
                handler:function(){
                    d.panel('close');
                }
            }]
        });
    }

    //删除
    function removeFunc(){
        var row = dg.datagrid('getSelected');
        if(rowIsNull(row)) return;
        parent.$.messager.confirm('提示', '删除后无法恢复，您确定要删除吗？', function(data){
            if (data){
                $.ajax({
                    type:'get',
                    url:"${ctx}/demo/community/remove/"+row.id,
                    success: function(data){
                        submitSuccess(data,dg);
                    }
                });
            }
        });
    }
    
    /**
     *查看课程详情
     */
    function view(){
    	var row = dg.datagrid('getSelected');
    	if(rowIsNull(row)) return;
    	d=$("#dlg").dialog({   
    	    title: '查看课程',    
    	    width: 900,    
    	    height: 700,    
    	    href:'${ctx}/demo/community/view/'+row.id,
    	    maximizable:true,
    	    modal:true,
    	    buttons:[]
    	});

    }
    
    //导出批号信息
    function exportFunc(){
    	$('#searchFrom').form('submit', {
    	    url:"${ctx}/demo/community/export",
    	    onSubmit: function(){
    			// do some check
    			// return false to prevent submit;
    	    },
    	    success:function(data){
    			
    	    }
    	});
    	 
    }

    //分页查询
    function searchFunc(){
        var formObj=$("#searchFrom").serializeObject();
        dg.datagrid('load',formObj);
    }

</script>
</body>
</html>